//
//  TableViewCell.swift
//  Newsfeed
//
//  Created by Dustin Mallory on 4/5/18.
//  Copyright © 2018 Dustin Mallory. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var presenterImageView: UIImageView!
    @IBOutlet weak var presenterActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var presenterLabel: UILabel!
    @IBOutlet weak var modifiedLabel: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var moreLabel: UILabel!
    @IBOutlet weak var primaryImageView: UIImageView!
    @IBOutlet weak var primaryActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var primaryErrorLabel: UILabel!
    @IBOutlet weak var reachedLabel: UILabel!
    @IBOutlet weak var responseLabel: UILabel!
    
    @IBOutlet weak var expandButton: UIButton!
    
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var commentButton: UIButton!
    
    @IBOutlet weak var loadingActivityIndicator: UIActivityIndicatorView!
}
