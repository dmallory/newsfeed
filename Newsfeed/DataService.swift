//
//  DataService.swift
//  Newsfeed
//
//  Created by Dustin Mallory on 4/5/18.
//  Copyright © 2018 Dustin Mallory. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class DataService: NSObject {

    static let instance = DataService()
    
    private let postEndpoint = "http://dev.findyourcohort.com/post"
    
    private let limit = 10
    
    private var nextUrl: String?
    
    private func postUrl(customer: Int, limit: Int, after: Int) -> String {
        return "\(postEndpoint)?customer_id=\(customer)&limit=\(limit)&after=\(after)"
    }
    
    func posts(customerId: Int, next: Bool = false, completion: ((PostResponse?) -> Void)?) {
        if let url = next ? nextUrl : postUrl(customer: customerId, limit: limit, after: 0) {
            nextUrl = nil
            Alamofire.request(url).responseObject { (response: DataResponse<PostResponse>) in
                if let post = response.result.value {
                    #if DEBUG
                    print(post.description)
                    #endif
                    self.nextUrl = post.paging?.next
                    completion?(post)
                } else {
                    if let error = response.result.error {
                        // Log to analytics or show an alert?
                        print("Error: \(error.localizedDescription)")
                    }
                    completion?(nil)
                }
            }
        }
    }
}
