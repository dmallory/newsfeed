//
//  PostResponse.swift
//  Newsfeed
//
//  Created by Dustin Mallory on 4/5/18.
//  Copyright © 2018 Dustin Mallory. All rights reserved.
//

import UIKit
import ObjectMapper

class PostResponse: Mappable {

    var paging: Paging?
    var data: Data?
    
    var description: String {
        return toJSONString(prettyPrint: true) ?? ""
    }
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        paging <- map["paging"]
        data <- map["data"]
    }
}

class Paging: Mappable {
    
    var next = ""
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        next <- map["next"]
    }
}

class Data: Mappable {
    
    var posts = [Post]()
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        posts <- map["post"]
    }
}

class Post: Mappable {
    
    var postId = 0
    var message = ""
    var primaryImageUrl: String?
    var thumbnailUrl: String?
    var creationTime = ""
    var lastModifiedTime = ""
    var customerId = 0
    var customerFirstName = ""
    var customerLastName = ""
    var presenterName = ""
    var presenterImageUrl = ""
    var totalPeopleReached = 0
    var totalResponses = 0
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        postId <- map["post_id"]
        message <- map["message"]
        primaryImageUrl <- map["primary_image_url"]
        thumbnailUrl <- map["thumbnail_url"]
        creationTime <- map["creation_time"]
        lastModifiedTime <- map["last_modified_time"]
        customerId <- map["customer_id"]
        customerFirstName <- map["customer_first_name"]
        customerLastName <- map["customer_last_name"]
        presenterName <- map["presenter_name"]
        presenterImageUrl <- map["presenter_image_url"]
        totalPeopleReached <- map["total_people_reached"]
        totalResponses <- map["total_responses"]
    }
}
