//
//  ViewController.swift
//  Newsfeed
//
//  Created by Dustin Mallory on 4/5/18.
//  Copyright © 2018 Dustin Mallory. All rights reserved.
//

import UIKit
import AlamofireImage
import LocalizedTimeAgo

class ViewController: UITableViewController {
    
    private let dataService = DataService.instance
    
    private var posts = [Post]()
    
    // Single hardcoded customer
    private let customerId = 1
    
    private var more = false
    
    private var expanded = [Int: Bool]()
    
    private var dateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        load()
    }
    
    func load(next: Bool = false) {
        dataService.posts(customerId: customerId, next: next) { (postResponse) in
            if let posts = postResponse?.data?.posts {
                if !next {
                    self.posts = posts
                } else {
                    self.posts.append(contentsOf: posts)
                }
                self.more = postResponse?.paging?.next != nil
                self.tableView.reloadData()
            } else {
                self.error(message: "Invalid posts response")
            }
        }
    }
    
    func error(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count + (more ? 1 : 0)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < posts.count {
            // Content height with or without image
            let post = posts[indexPath.row]
            return 390 - (post.primaryImageUrl != "" ? 0 : tableView.frame.size.width / 2) + ((expanded[indexPath.row] ?? false) ? 100 : 0)
        } else {
            // Loading height
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: indexPath.row >= posts.count ? "Loading" : "Post", for: indexPath) as! TableViewCell
        cell.presenterImageView?.layer.cornerRadius = cell.presenterImageView.frame.width / 2
        cell.presenterImageView?.clipsToBounds = true
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as! TableViewCell
        if cell.loadingActivityIndicator == nil {
            // Content cell
            let post = posts[indexPath.row]
            if let url = URL(string: post.presenterImageUrl) {
                // Show loading spinner while loading and display image with a nice transition once loaded
                cell.presenterImageView.image = nil
                cell.presenterActivityIndicator.startAnimating()
                cell.presenterImageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.1), runImageTransitionIfCached: false) { (image) in
                    // Hide spinner
                    cell.presenterActivityIndicator.stopAnimating()
                }
            } else {
                // Show default person icon without spinner
                cell.presenterActivityIndicator.stopAnimating()
                cell.presenterImageView.image = Image(named: "person")
            }
            cell.presenterLabel.text = post.presenterName
            
            cell.messageLabel.text = post.message
            
            if expanded[indexPath.row] ?? false {
                // Set height for expanded message
                cell.messageLabel.constraints.first?.constant = 140
                cell.messageLabel.numberOfLines = 10
                cell.moreLabel.isHidden = true
            } else {
                // If message is going to truncate past two lines show "... More"
                cell.messageLabel.constraints.first?.constant = 40
                cell.moreLabel.isHidden = checkLines(cell.messageLabel) < 3
            }
            
            cell.modifiedLabel.text = dateFormatter.date(from: post.lastModifiedTime)?.timeAgo()
        
            cell.reachedLabel.text = "\(post.totalPeopleReached) People Reached"
            cell.responseLabel.text = "\(post.totalResponses) Response\(post.totalResponses == 1 ? "" : "s")"
            
            cell.primaryImageView.image = nil
            cell.primaryErrorLabel.isHidden = true
            if let imageUrl = post.primaryImageUrl, let url = URL(string: imageUrl) {
                // Show loading spinner while loading and display image with a nice transition once loaded
                for constraint in cell.primaryImageView.constraints {
                    if constraint.identifier == "Height" {
                        constraint.constant = -1
                    }
                }
                cell.primaryActivityIndicator.startAnimating()
                cell.primaryImageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.1), runImageTransitionIfCached: false) { (response) in
                    // Hide spinner
                    cell.primaryActivityIndicator.stopAnimating()
                    if let _ = response.result.value {
                        // Image loaded
                    } else {
                        if let imageUrl = post.thumbnailUrl, let url = URL(string: imageUrl) {
                            // Try thumbnail instead
                            cell.primaryActivityIndicator.startAnimating()
                            cell.primaryImageView.af_setImage(withURL: url, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.1), runImageTransitionIfCached: false) { (response) in
                                cell.primaryActivityIndicator.stopAnimating()
                                if let _ = response.result.value {
                                    // Thumbnail loaded
                                } else {
                                    // Just show an error message
                                    cell.primaryErrorLabel.isHidden = false
                                }
                            }
                        } else {
                            // Just show an error message
                            cell.primaryErrorLabel.isHidden = false
                        }
                    }
                }
            } else {
                // Hide primary image
                for constraint in cell.primaryImageView.constraints {
                    if constraint.identifier == "Height" {
                        constraint.constant = 0
                    }
                }
            }
            
            cell.expandButton.tag = indexPath.row
            cell.expandButton.isHidden = cell.moreLabel.isHidden
        } else {
            // Loading cell
            cell.loadingActivityIndicator.startAnimating()
        }
        
        if indexPath.row > posts.count - 5 && more {
            load(next: true)
        }
    }
    
    func checkLines(_ label: UILabel) -> Int {
        let myText = label.text! as NSString
        let rect = CGSize(width: label.bounds.width, height: CGFloat.greatestFiniteMagnitude)
        let labelSize = myText.boundingRect(with: rect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: label.font], context: nil)
        return Int(ceil(CGFloat(labelSize.height) / label.font.lineHeight))
    }
    
    @IBAction func expandAction(_ sender: Any) {
        let row = (sender as! UIButton).tag
        expanded[row] = !(expanded[row] ?? false)
        tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
    }
}
